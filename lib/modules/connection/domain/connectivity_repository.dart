import 'dart:async';

import 'package:flutter_application_cep_search/modules/connection/data/connectivity_repository_impl.dart';

abstract class ConnectivityRepository {
  Stream<ConnectivityStatus> get status;

  void closeConnectivityStream();
}
