import 'package:flutter_application_cep_search/modules/connection/data/connectivity_repository_impl.dart';
import 'package:flutter_application_cep_search/modules/connection/domain/connectivity_repository.dart';

class ConnectionStatusUsecase {
  final ConnectivityRepository connectivityRepository;

  ConnectionStatusUsecase({
    required this.connectivityRepository,
  });

  Stream<ConnectivityStatus> call() {
    return connectivityRepository.status;
  }
}
