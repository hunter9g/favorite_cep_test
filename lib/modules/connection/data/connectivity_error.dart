class ConectivityError implements Exception {
  final String message;
  ConectivityError({
    required this.message,
  });
}
