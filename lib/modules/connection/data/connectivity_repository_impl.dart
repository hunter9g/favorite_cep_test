import 'dart:async';
import 'dart:developer';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_application_cep_search/modules/connection/domain/connectivity_repository.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

enum ConnectivityStatus { connected, disconnected }

class ConnectivityRepositoryImpl implements ConnectivityRepository {
  final _controller = StreamController<ConnectivityStatus>.broadcast();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  Stream<ConnectivityStatus> get status async* {
    Connectivity().onConnectivityChanged.listen(
      (ConnectivityResult result) async {
        log(result.toString());
        if (result != ConnectivityResult.none) {
          final connected = await InternetConnectionChecker().hasConnection;

          if (connected) {
            _controller.add(
              ConnectivityStatus.connected,
            );
          }
        } else {
          _controller.add(
            ConnectivityStatus.disconnected,
          );
        }
      },
    );
    yield* _controller.stream;
  }

  @override
  void closeConnectivityStream() {
    _connectivitySubscription.cancel();
  }
}
