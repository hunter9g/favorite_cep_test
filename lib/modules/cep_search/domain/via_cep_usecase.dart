import 'package:either_dart/either.dart';
import 'package:flutter_application_cep_search/modules/cep_search/data/errors/get_by_postalcode_error.dart';
import 'package:flutter_application_cep_search/modules/cep_search/data/model/search_by_cep_response_model.dart';
import 'package:flutter_application_cep_search/modules/cep_search/domain/via_cep_repository.dart';

class GetByPostalcodeViaCepUsecase {
  final ViaCepRepository viaCepRepository;

  GetByPostalcodeViaCepUsecase({
    required this.viaCepRepository,
  });

  Future<Either<GetByPostalCodeError, SearchByCepResponseModel>> call({
    required String postalcode,
  }) async {
    return viaCepRepository.getByPostalCode(
      postalcode: postalcode,
    );
  }
}
