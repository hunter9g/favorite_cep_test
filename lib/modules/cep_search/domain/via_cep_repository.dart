import 'package:either_dart/either.dart';
import 'package:flutter_application_cep_search/modules/cep_search/data/errors/get_by_postalcode_error.dart';
import 'package:flutter_application_cep_search/modules/cep_search/data/model/search_by_cep_response_model.dart';

abstract class ViaCepRepository {
  Future<Either<GetByPostalCodeError, SearchByCepResponseModel>>
      getByPostalCode({
    required String postalcode,
  });
}
