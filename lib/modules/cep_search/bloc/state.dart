import 'package:flutter_application_cep_search/modules/cep_search/data/model/search_by_cep_response_model.dart';
import 'package:flutter_application_cep_search/modules/cep_search/formz/cep_search_form_model.dart';

enum CEPSearchStatus { initial, loading, loaded, error, noInternet }

class CEPSearchState {
  final CEPSearchStatus status;
  final CEPSearchForm cepSearchForm;
  final SearchByCepResponseModel? searchByCepResponseModel;
  final String errorMessage;
  final List<SearchByCepResponseModel> responseModelList;

  CEPSearchState._({
    this.status = CEPSearchStatus.initial,
    required this.cepSearchForm,
    this.searchByCepResponseModel,
    this.errorMessage = '',
    this.responseModelList = const [],
  });

  CEPSearchState.initial()
      : this._(
          cepSearchForm: CEPSearchForm.empty,
        );

  CEPSearchState copyWith(
      {CEPSearchStatus? status,
      CEPSearchForm? cepSearchForm,
      SearchByCepResponseModel? searchByCepResponseModel,
      String? errorMessage,
      List<SearchByCepResponseModel>? responseModelList}) {
    return CEPSearchState._(
      searchByCepResponseModel: searchByCepResponseModel,
      status: status ?? this.status,
      cepSearchForm: cepSearchForm ?? this.cepSearchForm,
      errorMessage: errorMessage ?? this.errorMessage,
      responseModelList: responseModelList ?? this.responseModelList,
    );
  }
}
