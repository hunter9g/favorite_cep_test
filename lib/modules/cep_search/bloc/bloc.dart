import 'dart:async';
import 'dart:developer';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:either_dart/either.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/event.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/state.dart';
import 'package:flutter_application_cep_search/modules/cep_search/domain/via_cep_usecase.dart';
import 'package:flutter_application_cep_search/modules/cep_search/formz/cep_search_input_model.dart';
import 'package:flutter_application_cep_search/modules/connection/data/connectivity_repository_impl.dart';
import 'package:flutter_application_cep_search/modules/connection/domain/connectivity_status_usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

class CEPSearchBloc extends Bloc<CEPSearchEvent, CEPSearchState> {
  CEPSearchBloc({
    required GetByPostalcodeViaCepUsecase getByPostalcodeViaCepUsecase,
    required ConnectionStatusUsecase connectionStatusUsecase,
  }) : super(
          CEPSearchState.initial(),
        ) {
    _getByPostalcodeViaCepUsecase = getByPostalcodeViaCepUsecase;
    // starting helper connectivity
    _connectionStatusSubscription = connectionStatusUsecase.call().listen(
          (event) => add(
            OnConnectivityChanged(
              status: event,
            ),
          ),
        );
    on<FavoriteCEP>(
      _onFavoriteCEP,
    );
    on<OnConnectivityChanged>(
      _onConnectivityChanged,
    );
    on<SetSearchValue>(
      _setSearchValue,
    );
    on<SubmitCepSearch>(
      _submitCepSearch,
    );
  }

  late GetByPostalcodeViaCepUsecase _getByPostalcodeViaCepUsecase;
  late StreamSubscription<ConnectivityStatus> _connectionStatusSubscription;
  late StreamSubscription<ConnectivityResult>
      _connectivityStartStreamSubscriptionUsecase;

  _onFavoriteCEP(
    FavoriteCEP event,
    Emitter<CEPSearchState> emit,
  ) {
    emit(
      state.copyWith(
        responseModelList: [
          ...state.responseModelList,
          event.value,
        ],
      ),
    );
  }

  _onConnectivityChanged(
    OnConnectivityChanged event,
    Emitter<CEPSearchState> emit,
  ) {
    log(event.status.toString());
    if (event.status == ConnectivityStatus.disconnected) {
      emit(
        state.copyWith(
          status: CEPSearchStatus.noInternet,
          errorMessage: 'Não há internet!',
        ),
      );
    } else {
      emit(
        state.copyWith(
          status: CEPSearchStatus.initial,
          searchByCepResponseModel: null,
          errorMessage: '',
        ),
      );
    }
  }

  _setSearchValue(
    SetSearchValue event,
    Emitter<CEPSearchState> emit,
  ) {
    final search = CEPSearchInputFormz.dirty(
      value: event.value,
    );
    emit(
      state.copyWith(
        cepSearchForm: state.cepSearchForm.copyWith(
          status: FormzSubmissionStatus.initial,
          cepSearchInputFormz: search,
          isValid: Formz.validate(
            [
              search,
            ],
          ),
        ),
      ),
    );
  }

  @override
  Future<void> close() {
    _connectionStatusSubscription.cancel();
    _connectivityStartStreamSubscriptionUsecase.cancel();
    return super.close();
  }

  _submitCepSearch(
    SubmitCepSearch event,
    Emitter<CEPSearchState> emit,
  ) async {
    if (state.status == CEPSearchStatus.noInternet) {
      emit(
        state.copyWith(
          status: CEPSearchStatus.noInternet,
        ),
      );
    } else {
      if (state.cepSearchForm.isValid) {
        emit(
          state.copyWith(
            status: CEPSearchStatus.loading,
          ),
        );

        await Future.delayed(
          const Duration(seconds: 1),
        );

        final eitherResponse = _getByPostalcodeViaCepUsecase.call(
          postalcode: state.cepSearchForm.cepSearchInputFormz.value,
        );

        await eitherResponse.fold(
          (left) {
            emit(
              state.copyWith(
                status: CEPSearchStatus.error,
              ),
            );
          },
          (right) {
            emit(
              state.copyWith(
                status: CEPSearchStatus.loaded,
                searchByCepResponseModel: right,
              ),
            );
          },
        );

        emit(
          state.copyWith(
            status: CEPSearchStatus.loaded,
          ),
        );
      } else {
        emit(
          state.copyWith(
            cepSearchForm: state.cepSearchForm.copyWith(
              status: FormzSubmissionStatus.failure,
            ),
          ),
        );
      }
    }
  }
}
