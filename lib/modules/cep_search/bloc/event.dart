import 'package:flutter_application_cep_search/modules/cep_search/data/model/search_by_cep_response_model.dart';
import 'package:flutter_application_cep_search/modules/connection/data/connectivity_repository_impl.dart';

class CEPSearchEvent {}

class SetSearchValue extends CEPSearchEvent {
  final String value;
  SetSearchValue({
    required this.value,
  });
}

class FavoriteCEP extends CEPSearchEvent {
  final SearchByCepResponseModel value;
  FavoriteCEP({
    required this.value,
  });
}

class OnConnectivityChanged extends CEPSearchEvent {
  final ConnectivityStatus status;
  OnConnectivityChanged({
    required this.status,
  });
}

class SubmitCepSearch extends CEPSearchEvent {
  SubmitCepSearch();
}
