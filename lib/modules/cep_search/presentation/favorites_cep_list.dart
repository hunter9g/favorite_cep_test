import 'package:flutter/material.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/bloc.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoritesCepListPage extends StatelessWidget {
  const FavoritesCepListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lista de favoritos'),
      ),
      body: BlocBuilder<CEPSearchBloc, CEPSearchState>(
        buildWhen: (previous, current) =>
            previous.responseModelList.length !=
            current.responseModelList.length,
        bloc: context.read<CEPSearchBloc>(),
        builder: (context, state) => ListView.builder(
          itemCount: state.responseModelList.length,
          itemBuilder: (context, index) => ListTile(
            title: Text(
              state.responseModelList[index].bairro,
            ),
            subtitle: Text(
              '${state.responseModelList[index].logradouro}, ${state.responseModelList[index].cep}',
            ),
          ),
        ),
      ),
    );
  }
}
