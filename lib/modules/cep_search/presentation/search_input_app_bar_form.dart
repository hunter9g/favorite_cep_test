import 'package:flutter/material.dart';
import 'package:flutter_application_cep_search/modules/cep_search/components/search_input.dart';

class SearchInputAppBarForm extends AppBar {
  SearchInputAppBarForm({
    super.key,
    required bool enabled,
    required void Function()? onPressed,
  }) : super(
          centerTitle: true,
          toolbarHeight: 100,
          flexibleSpace: SafeArea(
              child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: const Icon(
                  Icons.list,
                ),
                onPressed: onPressed,
              ),
              Flexible(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 28.0,
                    ),
                    SizedBox(
                      height: 70,
                      child: CEPSearchInput(
                        enabled: enabled,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                width: 8.0,
              )
            ],
          )),
        );
}
