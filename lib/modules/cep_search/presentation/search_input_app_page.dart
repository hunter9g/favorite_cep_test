import 'package:flutter/material.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/bloc.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/event.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/state.dart';
import 'package:flutter_application_cep_search/modules/cep_search/presentation/favorites_cep_list.dart';
import 'package:flutter_application_cep_search/modules/cep_search/presentation/search_input_app_bar_form.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

class SearchInputAppPage extends StatelessWidget {
  const SearchInputAppPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CEPSearchBloc, CEPSearchState>(
      buildWhen: (previous, current) => previous.status != current.status,
      listener: (context, state) {
        if (state.cepSearchForm.status.isFailure) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text(
              'Parece que houve um erro com o formulário, verifique-o',
            ),
          ));
        }
        if (state.status == CEPSearchStatus.noInternet) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              state.errorMessage,
            ),
          ));
        }
      },
      builder: (context, state) => Scaffold(
        appBar: SearchInputAppBarForm(
          enabled: state.status != CEPSearchStatus.noInternet,
          onPressed: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => const FavoritesCepListPage(),
            ),
          ),
        ),
        body: state.status == CEPSearchStatus.loading
            ? const Center(
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Carregando..'),
                  ],
                ),
              )
            : state.searchByCepResponseModel != null
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 12.0,
                      vertical: 12.0,
                    ),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    state.searchByCepResponseModel!.cep,
                                  ),
                                  IconButton(
                                    onPressed: () =>
                                        context.read<CEPSearchBloc>().add(
                                              FavoriteCEP(
                                                value: state
                                                    .searchByCepResponseModel!,
                                              ),
                                            ),
                                    icon: const Icon(
                                      Icons.favorite,
                                    ),
                                  )
                                ]),
                            Text(
                              state.searchByCepResponseModel!.bairro,
                            ),
                            Text(
                              state.searchByCepResponseModel!.logradouro,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                : const Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Estado inicial',
                        ),
                        // CEPSearchInput(),
                      ],
                    ),
                  ),
      ),
    );
  }
}
