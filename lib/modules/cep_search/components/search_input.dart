import 'package:flutter/material.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/bloc.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/event.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/state.dart';
import 'package:flutter_application_cep_search/modules/cep_search/formz/cep_search_input_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CEPSearchInput extends StatelessWidget {
  final bool enabled;
  const CEPSearchInput({
    super.key,
    required this.enabled,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CEPSearchBloc, CEPSearchState>(
      bloc: context.read<CEPSearchBloc>(),
      buildWhen: (previous, current) =>
          previous.cepSearchForm.cepSearchInputFormz.value !=
          current.cepSearchForm.cepSearchInputFormz.value,
      builder: (context, state) => SizedBox(
        child: TextField(
          enabled: enabled,
          maxLength: 8,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            suffixIcon: IconButton(
              icon: const Icon(Icons.search),
              onPressed: () => context.read<CEPSearchBloc>().add(
                    SubmitCepSearch(),
                  ),
            ),
            isDense: true,
            filled: true,
            fillColor: const Color(0xfff1f1f1),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide.none,
            ),
            prefixIcon: const Icon(Icons.search),
            prefixIconColor: Colors.black,
            hintText: 'Digite aqui seu cep..',
            errorText: state.cepSearchForm.cepSearchInputFormz.isValid
                ? null
                : state.cepSearchForm.cepSearchInputFormz.displayError ==
                        CEPSearchInputError.empty
                    ? 'Campo vazio'
                    : state.cepSearchForm.cepSearchInputFormz.displayError ==
                            CEPSearchInputError.eightCharactersLong
                        ? 'O campo precisa ter ao menos oito caracteres'
                        : null,
          ),
          onChanged: (value) => context.read<CEPSearchBloc>().add(
                SetSearchValue(
                  value: value,
                ),
              ),
        ),
      ),
    );
  }
}
