import 'package:flutter_application_cep_search/modules/cep_search/data/via_cep_datasource.dart';
import 'package:http/http.dart' as http;

class ViaCepDatasourceImpl implements ViaCepDatasource {
  static const baseURL = 'https://viacep.com.br/ws/';
  @override
  getByPostalCode({required String postalcode}) async {
    return http.get(
      Uri.parse('$baseURL/$postalcode/json/'),
    );
  }
}
