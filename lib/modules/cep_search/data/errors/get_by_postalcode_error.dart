class GetByPostalCodeError implements Exception {
  final String message;
  GetByPostalCodeError({
    required this.message,
  });
}
