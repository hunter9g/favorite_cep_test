import 'package:either_dart/either.dart';
import 'package:flutter_application_cep_search/modules/cep_search/data/errors/get_by_postalcode_error.dart';
import 'package:flutter_application_cep_search/modules/cep_search/data/model/search_by_cep_response_model.dart';
import 'package:flutter_application_cep_search/modules/cep_search/data/via_cep_datasource.dart';
import 'package:flutter_application_cep_search/modules/cep_search/domain/via_cep_repository.dart';

class ViaCepRepositoryImpl implements ViaCepRepository {
  final ViaCepDatasource viaCepDatasource;

  ViaCepRepositoryImpl({
    required this.viaCepDatasource,
  });

  @override
  Future<Either<GetByPostalCodeError, SearchByCepResponseModel>>
      getByPostalCode({
    required String postalcode,
  }) async {
    try {
      final response = await viaCepDatasource.getByPostalCode(
        postalcode: postalcode,
      );

      final responseModel = SearchByCepResponseModel.fromJson(
        response.body,
      );

      return Right(
        responseModel,
      );
    } catch (e) {
      return Left(
        GetByPostalCodeError(
          message: e.toString(),
        ),
      );
    }
  }
}
