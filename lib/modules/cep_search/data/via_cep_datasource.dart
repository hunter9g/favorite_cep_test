import 'package:http/http.dart' as http;

abstract class ViaCepDatasource {
  Future<http.Response> getByPostalCode({required String postalcode});
}
