import 'dart:convert';

import 'package:flutter_application_cep_search/modules/cep_search/data/entity/search_by_cep_response_entity.dart';

class SearchByCepResponseModel extends SearchByCepResponseEntity {
  SearchByCepResponseModel({
    required super.cep,
    required super.logradouro,
    required super.complemento,
    required super.bairro,
    required super.localidade,
  }) : super(
          uf: '',
          ibge: '',
          gia: '',
          ddd: '',
          siafi: '',
        );

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'cep': cep,
      'logradouro': logradouro,
      'complemento': complemento,
      'bairro': bairro,
      'localidade': localidade,
      'uf': uf,
      'ibge': ibge,
      'gia': gia,
      'ddd': ddd,
      'siafi': siafi,
    };
  }

  factory SearchByCepResponseModel.fromMap(Map<String, dynamic> map) {
    return SearchByCepResponseModel(
      cep: map['cep'] as String,
      logradouro: map['logradouro'] as String,
      complemento: map['complemento'] as String,
      bairro: map['bairro'] as String,
      localidade: map['localidade'] as String,
    );
  }

  String toJson() => json.encode(
        toMap(),
      );

  factory SearchByCepResponseModel.fromJson(String source) =>
      SearchByCepResponseModel.fromMap(
        json.decode(source) as Map<String, dynamic>,
      );
}
