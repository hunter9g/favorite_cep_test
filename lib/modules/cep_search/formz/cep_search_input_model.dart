import 'package:formz/formz.dart';

enum CEPSearchInputError {
  empty,
  eightCharactersLong,
  maxLimit,
}

class CEPSearchInputFormz extends FormzInput<String, CEPSearchInputError> {
  const CEPSearchInputFormz.pure() : super.pure('');

  const CEPSearchInputFormz.dirty({String value = ''}) : super.dirty(value);

  @override
  CEPSearchInputError? validator(String value) {
    if (value.isEmpty) {
      return CEPSearchInputError.empty;
    }
    if (value.length < 8) {
      return CEPSearchInputError.eightCharactersLong;
    }
    if (value.length > 8) {
      return CEPSearchInputError.maxLimit;
    }
    return null;
  }
}
