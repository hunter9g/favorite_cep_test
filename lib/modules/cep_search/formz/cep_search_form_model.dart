// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:formz/formz.dart';

import 'package:flutter_application_cep_search/modules/cep_search/formz/cep_search_input_model.dart';

class CEPSearchForm {
  final FormzSubmissionStatus status;
  final CEPSearchInputFormz cepSearchInputFormz;
  final bool isValid;
  CEPSearchForm._({
    required this.cepSearchInputFormz,
    required this.status,
    required this.isValid,
  });

  CEPSearchForm copyWith({
    FormzSubmissionStatus? status,
    CEPSearchInputFormz? cepSearchInputFormz,
    bool? isValid,
  }) {
    return CEPSearchForm._(
      status: status ?? this.status,
      cepSearchInputFormz: cepSearchInputFormz ?? this.cepSearchInputFormz,
      isValid: isValid ?? this.isValid,
    );
  }

    static CEPSearchForm get empty => CEPSearchForm._(
      status: FormzSubmissionStatus.initial,
      cepSearchInputFormz: const CEPSearchInputFormz.pure(),
      isValid: false,
    );
}
