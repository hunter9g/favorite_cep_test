import 'package:flutter/material.dart';
import 'package:flutter_application_cep_search/injection_container.dart';
import 'package:flutter_application_cep_search/modules/cep_search/bloc/bloc.dart';
import 'package:flutter_application_cep_search/modules/cep_search/presentation/search_input_app_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  setup();
  // WidgetsFlutterBinding.ensureInitialized();
  runApp(
    const MainApp(),
  );
}

class MainApp extends StatelessWidget {
  const MainApp({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CEPSearchBloc>(
          create: (BuildContext context) => getIt<CEPSearchBloc>(),
        )
      ],
      child: const MaterialApp(
        home: SearchInputAppPage(),
      ),
    );
  }
}
