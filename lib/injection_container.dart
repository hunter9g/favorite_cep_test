import 'package:flutter_application_cep_search/modules/cep_search/bloc/bloc.dart';
import 'package:flutter_application_cep_search/modules/cep_search/data/repository/via_cep_repository_impl.dart';
import 'package:flutter_application_cep_search/modules/cep_search/data/via_cep_datasource.dart';
import 'package:flutter_application_cep_search/modules/cep_search/domain/via_cep_repository.dart';
import 'package:flutter_application_cep_search/modules/cep_search/domain/via_cep_usecase.dart';
import 'package:flutter_application_cep_search/modules/cep_search/external/via_cep/via_cep_datasource_impl.dart';
import 'package:flutter_application_cep_search/modules/connection/data/connectivity_repository_impl.dart';
import 'package:flutter_application_cep_search/modules/connection/domain/connectivity_repository.dart';
import 'package:flutter_application_cep_search/modules/connection/domain/connectivity_status_usecase.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setup() {
  getIt.registerSingleton<ViaCepDatasource>(
    ViaCepDatasourceImpl(),
  );

  getIt.registerSingleton<ViaCepRepository>(
    ViaCepRepositoryImpl(
      viaCepDatasource: getIt<ViaCepDatasource>(),
    ),
  );

  getIt.registerSingleton<GetByPostalcodeViaCepUsecase>(
    GetByPostalcodeViaCepUsecase(
      viaCepRepository: getIt<ViaCepRepository>(),
    ),
  );

  getIt.registerSingleton<ConnectivityRepository>(
    ConnectivityRepositoryImpl(),
  );

  getIt.registerSingleton<ConnectionStatusUsecase>(
    ConnectionStatusUsecase(
      connectivityRepository: getIt<ConnectivityRepository>(),
    ),
  );

  getIt.registerSingleton<CEPSearchBloc>(
    CEPSearchBloc(
      getByPostalcodeViaCepUsecase: getIt<GetByPostalcodeViaCepUsecase>(),
      connectionStatusUsecase: getIt<ConnectionStatusUsecase>(),
    ),
  );
}
